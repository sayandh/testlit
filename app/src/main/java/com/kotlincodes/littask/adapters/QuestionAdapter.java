package com.kotlincodes.littask.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kotlincodes.littask.R;
import com.kotlincodes.littask.model.QuestionModel;
import com.kotlincodes.littask.utilis.Utilities;

import java.util.ArrayList;
import java.util.List;


public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.ViewHolder> {
    private Context context;
    private List<QuestionModel.Items> questionList;

    public QuestionAdapter(Context context, List<QuestionModel.Items> quetionList) {
        this.context = context;
        this.questionList = quetionList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_questions, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final QuestionModel.Items rowItem = questionList.get(position);
        holder.titleTxt.setText(rowItem.getTitle());
        holder.dateTxt.setText(Utilities.getDate(rowItem.getCreationDate()));
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTxt;
        TextView dateTxt;
        CardView cardFull;

        public ViewHolder(View itemView) {
            super(itemView);
            titleTxt = itemView.findViewById(R.id.questiion_title);
            dateTxt = itemView.findViewById(R.id.questiion_time);

        }
    }



}
