package com.kotlincodes.littask.retrofit;

import com.kotlincodes.littask.model.QuestionModel;
import com.kotlincodes.littask.utilis.Utilities;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface
{
        @GET("questions?key="+ Utilities.API_KEY+"&sort=activity&order=desc&site=stackoverflow&filter=withbody")
    Call<QuestionModel> getQuestions();

    @GET("questions/{question_id}?key=f)yov8mEGrYZa1dJDb2gpg((&site=stackoverflow&filter=withbody")
    Call<QuestionModel> getDetails(@Path(value = "question_id", encoded = true) String questionID);

//    @GET("/maps/api/place/nearbysearch/json?key=AIzaSyDrl_Ol85Qy2B-heR2VkRKr_lbKtFQAoVg")
//    Call<NearbyModel>getNearby(@Query("radius") String radius, @Query("location") String location, @Query("type") String type, @Query("pagetoken") String nextPageToken,
//                               @Query("keyword") String keyword);
//
//    @GET("/maps/api/place/details/json?key=AIzaSyDrl_Ol85Qy2B-heR2VkRKr_lbKtFQAoVg")
//    Call<PlaceDetailsModel> getPlaceDetails(@Query("placeid") String placeId);
//
//    @GET("/maps/api/directions/json?key=AIzaSyDrl_Ol85Qy2B-heR2VkRKr_lbKtFQAoVg")
//    Call<DirectionModel> getDirection(@Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode);
}
