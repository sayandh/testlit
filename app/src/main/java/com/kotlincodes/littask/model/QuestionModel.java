package com.kotlincodes.littask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuestionModel {

    @Expose
    @SerializedName("quota_remaining")
    private int quotaRemaining;
    @Expose
    @SerializedName("quota_max")
    private int quotaMax;
    @Expose
    @SerializedName("has_more")
    private boolean hasMore;
    @Expose
    @SerializedName("items")
    private List<Items> items;

    public int getQuotaRemaining() {
        return quotaRemaining;
    }

    public int getQuotaMax() {
        return quotaMax;
    }

    public boolean getHasMore() {
        return hasMore;
    }

    public List<Items> getItems() {
        return items;
    }

    public static class Owner {
        @Expose
        @SerializedName("link")
        private String link;
        @Expose
        @SerializedName("display_name")
        private String displayName;
        @Expose
        @SerializedName("profile_image")
        private String profileImage;
        @Expose
        @SerializedName("user_type")
        private String userType;
        @Expose
        @SerializedName("user_id")
        private int userId;
        @Expose
        @SerializedName("reputation")
        private int reputation;

        public String getLink() {
            return link;
        }

        public String getDisplayName() {
            return displayName;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public String getUserType() {
            return userType;
        }

        public int getUserId() {
            return userId;
        }

        public int getReputation() {
            return reputation;
        }
    }

    public static class Items {
        @Expose
        @SerializedName("body")
        private String body;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("closed_reason")
        private String closedReason;
        @Expose
        @SerializedName("link")
        private String link;
        @Expose
        @SerializedName("question_id")
        private int questionId;
        @Expose
        @SerializedName("last_edit_date")
        private int lastEditDate;
        @Expose
        @SerializedName("creation_date")
        private int creationDate;
        @Expose
        @SerializedName("last_activity_date")
        private int lastActivityDate;
        @Expose
        @SerializedName("score")
        private int score;
        @Expose
        @SerializedName("answer_count")
        private int answerCount;
        @Expose
        @SerializedName("closed_date")
        private int closedDate;
        @Expose
        @SerializedName("view_count")
        private int viewCount;
        @Expose
        @SerializedName("is_answered")
        private boolean isAnswered;
        @Expose
        @SerializedName("owner")
        private Owner owner;
        @Expose
        @SerializedName("tags")
        private List<String> tags;

        public String getBody() {
            return body;
        }

        public String getTitle() {
            return title;
        }

        public String getClosedReason() {
            return closedReason;
        }

        public String getLink() {
            return link;
        }

        public int getQuestionId() {
            return questionId;
        }

        public int getLastEditDate() {
            return lastEditDate;
        }

        public int getCreationDate() {
            return creationDate;
        }

        public int getLastActivityDate() {
            return lastActivityDate;
        }

        public int getScore() {
            return score;
        }

        public int getAnswerCount() {
            return answerCount;
        }

        public int getClosedDate() {
            return closedDate;
        }

        public int getViewCount() {
            return viewCount;
        }

        public boolean getIsAnswered() {
            return isAnswered;
        }

        public Owner getOwner() {
            return owner;
        }

        public List<String> getTags() {
            return tags;
        }
    }
}
