package com.kotlincodes.littask.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.kotlincodes.littask.R;
import com.kotlincodes.littask.adapters.QuestionAdapter;
import com.kotlincodes.littask.model.QuestionModel;
import com.kotlincodes.littask.retrofit.ApiClient;
import com.kotlincodes.littask.utilis.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private List<QuestionModel.Items> listQuestions=new ArrayList<>();
    private QuestionAdapter adapter;
    private LinearLayout linearLayoutProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar=findViewById(R.id.toolbar);
        RecyclerView recyclerViewQ = findViewById(R.id.recycler_questions);
        linearLayoutProgress=findViewById(R.id.layout_progress);

        setSupportActionBar(toolbar);
        setTitle("TestLit");

        adapter=new QuestionAdapter(this,listQuestions);
        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        recyclerViewQ.setAdapter(adapter);
        recyclerViewQ.setLayoutManager(layoutManager);

        recyclerViewQ.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerViewQ, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent=new Intent(MainActivity.this,DetailsActivity.class);
                intent.putExtra("QuestionID",String.valueOf(listQuestions.get(position).getQuestionId()));
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        getQuestoions();
    }
    private void getQuestoions() {
        Call<QuestionModel> call = ApiClient.getClient().getQuestions();
        call.enqueue(new Callback<QuestionModel>() {
            @Override
            public void onResponse(@NonNull Call<QuestionModel> call, @NonNull Response<QuestionModel> response) {
                if (response.isSuccessful()) {
                    linearLayoutProgress.setVisibility(View.GONE);
                    assert response.body() != null;
                    listQuestions.addAll(response.body().getItems());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<QuestionModel> call, Throwable t) {
            }
        });
    }

}
