package com.kotlincodes.littask.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kotlincodes.littask.R;
import com.kotlincodes.littask.model.QuestionModel;
import com.kotlincodes.littask.retrofit.ApiClient;

import org.jsoup.Jsoup;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsActivity extends AppCompatActivity {
    private TextView webView;
    private TextView titleTxt;
    LinearLayout laoutLoading;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        String quetionID=getIntent().getStringExtra("QuestionID");
        setTitle("Question Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        laoutLoading=findViewById(R.id.layout_progress_details);
        webView=findViewById(R.id.txt_details);
        titleTxt=findViewById(R.id.txt_title);
        getDetails(quetionID);
    }

    private void getDetails(String questionID) {
        Call<QuestionModel> call = ApiClient.getClient().getDetails(questionID);
        call.enqueue(new Callback<QuestionModel>() {
            @Override
            public void onResponse(@NonNull Call<QuestionModel> call, @NonNull Response<QuestionModel> response) {
                laoutLoading.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getItems().size()!=0){
                        webView.setText(Jsoup.parse(response.body().getItems().get(0).getBody()).text());
                        titleTxt.setText(response.body().getItems().get(0).getTitle());
                    }
                }
            }

            @Override
            public void onFailure(Call<QuestionModel> call, Throwable t) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return true;
    }
}
